import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Routes, Route} from 'react-router-dom'
import {Fragment} from 'react'
import {useState} from 'react'
import AppNavbar from './Components/AppNavbar';
import Home from './pages/home'
import Logout from './pages/logout'
import Courses from './pages/courses'
import Register from './pages/register'
import Login from './pages/login'
import NotFound from './pages/notfound'
import './App.css';
import { UserProvider } from './UserContext'







function App() {
  
  //State hook for the user state that's defined here for a global scope 
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear()
  }



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
    <AppNavbar/>
    <Container>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/courses" element={<Courses/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="*" element={<NotFound/>}/>
          </Routes>
          </Container>
    </Router>
    </UserProvider>



  );
}

export default App;
