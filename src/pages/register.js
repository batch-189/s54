import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import { Navigate} from 'react-router-dom'
import UserContext from '../UserContext'


export default function Register(){
	
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	//State to determine when the sumbit button is enabled or not
	const [isActive, setIsActive] = useState(false)
	const {user, setUser} = useContext(UserContext)
	// console.log(email)
	// console.log(password1)
	// console.log(password2)
	



	function registerUser(e){
		
		e.preventDefault()

		localStorage.setItem("email", email)

		//set the global user state to have properties obtained from local storage
		setUser({
			email: localStorage.getItem('email')
		})


		setEmail("");
		setPassword1("")
		setPassword2("")




		alert("You are now registered!")





	}

	useEffect(() => {
		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			

			setIsActive(true)
		}else{
			setIsActive(false)
		}

	}, [email, password1, password2])


	return(
		(user.email !== null)?
		<Navigate to="/courses"/>
		:


		<Form className="mt-3" onSubmit={(e)=> registerUser(e)}>
			<h1 className="text-center">Register</h1>



		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
			        type="email" 
			        placeholder="Enter email"
			        value={email}
			        onChange={e => {
			        	setEmail(e.target.value)
			        	console.log(e.target.value)
			        }} 
			        required/>

		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        type="password"
		        value={password1}
		        onChange={e => {
			        	setPassword1(e.target.value)
			        	
			        }} 
		        placeholder="Password" />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        type="password"
		        value={password2} 
		        onChange={e => setPassword2(e.target.value)}
		        placeholder="Password" />
		      </Form.Group>

		      {
		      	isActive ?
		      	<Button variant="primary" type="submit" id="sumbitBtn">
		        Submit
		      </Button>
		      	:
		      	<Button variant="danger" type="submit" id="sumbitBtn" disabled>
		        Submit
		      </Button>
		      }



		      
		      
    </Form>


		)
}